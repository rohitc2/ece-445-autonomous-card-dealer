# Rohit Chalamala's ECE 445 Lab Notebook


## 01/17/2023

Today was the first ECE 445 lecture of the semester, it was pretty straightforward, going over some syllabus stuff, hearing some project pitches for some things that we could potentially do this semester, and lastly starting brainstorming some ideas for the project this semester. As far as partners go, coming into this class I only had my roommate Adam, but today we went around and talked to people and newtworked and found a few potential partners. 

In terms of brainstorming, we had a few different ideas including:
- A leaf raking robot (A robot that you would put in your lawn, and it would go and rake up all of the leaves and place them in a pile for you to pick up.)
- A wall painting robot (A robot that would make painting houses way easier by painting walls efficiently and autonomously.)
- A drone that follows you around (A drone that would use facial recognition to identify you and then just follow you around.)

After brainstorming these ideas, I decided to go talk to one of the TAs regarding the wall painting robot, he said that it could be feasible if we did more research on it. At this point, I figured it might be a good idea to begin doing some research on the idea since the Initial Web Board Post deadline was coming up. The problems that I found with the idea were in regards to how it would stick to the walls and how it would paint precisely and accurately. However, I decided to post on the forum anyways to get some feedback and see if there was any ways to combat these problems.
<br/><br/> 


## 01/22/2023

Today my partner and I continued doing some brainstorming on each of the ideas we posted about. In particular we researched about the leaf raking robot and the wall painting robot as that is what we made our initial web board posts about. Also I received some replies on my web board post with concerns such as how the device would be able to stick to the wall without daming it, and the cost versus payoff of this project with its complexity. In terms of my findings of how to combat these problems, I modified my idea a little bit to account for these things. My idea now is to create a robot that will be on the ground, such that it can increase and decrease height using some type of mechanism, it will use spray painting but close to the wall to make sure it is more accurate. However, I still don't know if this idea is feasible, I will continue doing research on it. I plan to try and better understand this idea soon so that I can maybe push for early project approval.

Next, I decided to do the Laboratory Safety Training. This wasn't very difficult as I have done it before a few years in advance for Illini Solar Car, though my certificate did expire so I had to redo it. I just went through all the modules and understood what safety hazards might be present, and what the correct actions are for various different situations. 
<br/><br/> 


## 01/24/2023

My partner and I ended up finding another partner, it was someone who we had initially talked to during the first class, his name is Ralph. We decided to brainstorm some more but as an entire group to see if we could come up with some more ideas. Ralph proposed an idea that he had for an autonomous card dealer. This project would basically be able to deal the cards out to the players in some way for a game like poker for example. I personally really liked this idea and Ralph also had a few good comments on his web board post that seemed to indicate that this project would be doable. Based on this, we decided to move this to the top of our projects list, though we continued brainstorming for a while but we weren't able to come up with anything we liked more. 
<br/><br/> 


## 01/25/2023

We worked on the RFA for the autonomous card dealer. While we were brainstorming though we ended up modifying the project and turning it into an autonomous card shuffler and dealer as we felt it would be a more useful device compared to just the dealer. 

This is the general outline of our project as we described in our RFA:
- We want to create an autonomous card shuffler and dealer to make the card playing process more fair and effortless.
- There are 3 main components to the project: a shuffler, a dealer, and a user interface.
- There are 3 main criterion for success that we have: shuffle a set of cards evenly, distribute the cards to the players, and have buttons for the user interface.

We also did some research on some different parts that we might be able to use to help make project this work. However, these parts aren't final and are just ones we found from initial research, we plan to look more into it. 
<br/><br/> 


## 01/26/2023 - 01/27/2023

Today, I worked mainly on the schematic portion of the CAD assignment and also started on the PCB portion but I didn't finish it. I haven't used KiCad in a few years so it took me a while to get accustomed to, also the scrolling and zooming in mechanisms in KiCad are very frustrating so it took a bit to learn it. After I learned how to use the software, I began just following the guide that was provided on the course website. 

There were 4 main sections to the schematic: Microcontroller, ISP, Power and Debouncing. After placing all of the pieces down and organizing everything into a presentable format, I began drawing the lines between components to connect them. After all 4 of the sections were done, I rearranged some things, drew lines between each section, and labeled each section as shown in the assignment tutorial. I also lastly added some mounting holes that I believe will be used for the PCB later. The last thing I had to do for the PCB is running component association to give each of the pieces footprints, I just followed what was shown on the tutorial.

Next, I started on the PCB, but I have never made a PCB before so this was a little more difficult than the schematic portion. I placed the parts on the PCB tool and then rearranged them in a similar manner to what was shown on the website. Since I had mostly arranged everything the way that they had in the tutorial, the next step was to add the title, date, and my name to the PCB design. At this point, I found this cool thing on KiCad called the 3D Viewer, it was really interesting to see whatever I was building in a more understandable way. At this point came the most difficult part of the assignment for me, I had to enclose my parts in a box, put the mounting holes on the edges, and then make sure that the distance between the mounting holes was exactly as specified in the instructions. This took a really long time because I was having trouble moving it the exact amount to get it to be the same distance. I also realized my parts were too far spread at some point so I had to condense everything, and then move all my mounting holes again and repeat the process. However, eventually I was able to figure it out and get something similar to the example and with the correct dimensions. The next step was to add tracks to the PCB design, but this was mostly just following what the picture had on it, so it was fairly straightforward. There were some tracks that had to be different sizes than other ones, I think based on power consumption but I'm not entirely sure. Anyways, I just copied what was shown on the image for track sizes. After all the tracks had been laid out, I had to add some grounds across the board to make sure everything functions properly. As a whole, the process of making the schematic and PCB were very fulfilling as it has always been something that I've wanted to do, even though some portions of it were annoying. 
<br/><br/> 


## 02/06/2023

Today, I went into the lab to work on the Soldering assignment. I wasn't expecting this assignment to take nearly as long as it did, I have done some minimal soldering before, but a long time ago. At first, I watched some videos on how soldering works and what the tools do and what you need in order to solder. I learned that you need to use the hot soldering tool to melt the solder material onto the components that you want to attach to your PCB. I also learned that the sponge needs to be wet to clean off your tool. The last thing that I learned was that you can remove soldering material by using a vacuum type pump device that will suck in all of the solder material if you heat it up. After I learned all this, I grabbed the bag of materials and tried to actually start soldering. It was really hard due to how small all of the parts are, I kept messing up how the parts were oriented and how much solder I put on certain areas. At one point, I ended up frying my board due to the heat, so I had to restart with a new board. However, after a while it became easier when I used the tweezers. I also ended up switching stations become my tool was really blunt, so I found a pointier one which made the process a lot easier. Eventually, I was able to finish soldering, though later in the assignment there were a few connections that I had to fix. After I had finished soldering, I then had to program my PCB with the code that was provided on the website. At this point, I had to build the encabulator which was pretty straightforward outside of crimping the wires which took a little bit to figure out. At this point, I just plugged my PCB into the encabulator, and everything worked as expected after I fixed a few of the soldering connections. I definitely learned a lot from this assignment, though I'm kind of scared for when we have to solder for our own PCBs because I feel like it'll be a lot bigger than this one. 
<br/><br/> 


## 02/07/2023 - 02/08/2023

Today was our first meeting with our TA, it was a pretty informal meeting, mostly focused around discussing the card dealer and shuffler project and what our thoughts/ideas on how to implement the project are. We also did a lot more research on the project in regards to how we should set up the system so that we can both shuffle and deal. Simultaneously while we were doing more research on the project, we also decided it would be a good idea to work on the project proposal. We found some inspiration for the shuffling aspect of our project from some of the shufflers that we saw online. The basic concept for these was to put half the deck on each side of this boxed area that motors would shoot the cards into one at a time. This seemed like a fairly simple idea that would only require two motors so we decided to go with that. As for the dealing aspect, our idea is to have a motor under where the cards are deposited, this will allow us to shoot them out. We decided that we would make a very small slit, maybe the width of 1.75 cards or so in order to make it such that only 1 card can be shot out a time to improve the overall card dealing process. As for our rotating mechanics, we just figured it would be easiest to place a motor underneath that rotates in a circular motion. Outside of this stuff, we will have an ultrasonic sensor to determine the distance away that a certain person might be, and we will have some type of UI that users can use. We also came up with some requirements for the project which are shuffling a deck of cards evenly, distributing the cards to the players and setting up the field, and having an easy to use UI. We will have 4 main subsystems, the power subsystem, the user interface subsystem, the sensing subsystem, and the motor subsystem. We created a visual aid that shows how the project should look, we also created a block diagram for the subsystems, both of which I will include here. There were a few other aspects of the project proposal, but those were the main ones.
<br/><br/> 

![This is the visual aid for the card dealer and shuffler that we created for the project proposal](Visual_Aid.png)

<br/><br/> 

This is the visual aid for the card dealer and shuffler that we created for the project proposal.

<br/><br/> 

![This is the block diagram for the card dealer and shuffler that we created for the project proposal](Block_Diagram.png)

<br/><br/> 

This is the block diagram for the card dealer and shuffler that we created for the project proposal.

<br/><br/> 


## 02/15/2023 - 02/16/2023

We realized that we had missed the Machine Shop deadline as we had not seen it on the calendar. We went to the Machine Shop and spoke with Gregg who told us that it might not be possible to complete our project due to the mechanical complexity, so we did some brainstorming on how we could possibly reduce the amount of work they had to do. Our first idea was to scrap a portion of the project so that they wouldn't have to build as much. However, we eventually came up with the idea that we could buy a card shuffler online so that they could use the enclosure and they would be able to build around that. We got this idea approved by the Machine Shop and our TA and decided to proceed with. We are obviously still planning on doing all of the electrical work, we will just give them the parts so they can build what we need for our project as far as the mechanical work goes. We also decided to fix the project proposal because we didn't do very well on the first attempt of it. We were missing a portion of the document in the previous iteration, the ethics and safety section. We also didn't have the most descriptive tolerance analysis and subsystem requirements sections which we also lost points for. We decided it was better to fix it early so that we will be set up well for when we want to start on the design document, which is likely going to be tomorrow as there is a lot of information to cover. 
<br/><br/> 


## 02/17/2023 - 02/21/2023

We decided that it was better to start working on the design document early in order to ensure that we can get in as much information as possible so that we don't lose as many points as we did in the project proposal. To be honest, this design document took a really really long time to write just because of how large our project is and all of the moving parts that need to each be succesful for our entire project to work. Some of the things that we had to do included determining a good way to show our project as a visual aid, improving on our tolerance analysis, creating a schematic for the entire project and picking parts out, determining the cost of our project, and also creating R&V tables for each subsystem. The schematic was fairly difficult as we didn't have a great idea what parts we wanted to use yet, but we have a general idea so we used that to determine how everything in the schematic should be set up. The longest part of this project was definitely the R&V tables just because we had so many requirements for each subsystem. This meant that we had to figure out how to verify each requirement which took a while to come up with especially for some of the motor subsystem ones as we didn't really understand a good way to test it. Eventually we were able to figure it out though. Across a few days, we were able to do all that we needed to have the design document basically done which hopefully sets us up well for the coming weeks. 
<br/><br/> 

![This is our schematic that we've come up with for our project](Schematic.png)

<br/><br/> 

This is our schematic that we've come up with for our project.

<br/><br/> 

![This is the visual aid for how the sections of the project are set up, the top one shows the card shuffler enclosure that we will be using, the next one is a visual aid of the middle section of the project that we will be shooting our card from, the last part is the rotating base that we will be using which the Machine Shop gave us](Project_Sections.png)

<br/><br/> 

This is the visual aid for how the sections of the project are set up, the top one shows the card shuffler enclosure that we will be using, the next one is a visual aid of the middle section of the project that we will be shooting our card from, the last part is the rotating base that we will be using which the Machine Shop gave us.
<br/><br/> 


## 02/21/2023 - 02/22/2023

We met with the machine shop again in order to explain our ideas and design. We had a better understanding now since we had already finished a majority of our design document. We were able to show them a physical design sketch that we created which seemed to help them understand what we were trying to do a little better. We were told that it would be feasible, and Gregg told us that he would contact us again when he finds someone to take on our project. After this we finished up the remainder of our design document which was some edits on the R&V tables along with general formatting stuff and adding some small details throughout the document. After that, we also did the team contract which was pretty straightforward and simple. 

<br/><br/> 

![This is the physical design that we used for our design document and also used to explain our project better to the machine shop](Physical_Design.png)

<br/><br/>

This is the physical design that we used for our design document and also used to explain our project better to the machine shop.
<br/><br/> 


## 02/25/2023 - 02/26/2023

We realized that our entire schematic is wrong because we were using a NUCLEO-F411RE Development Board, but we aren't allowed to use development boards in this class. At this point, we were trying to figure out what to do, and we decided to switch to the STMF103C8T6 Microcontroller Chip. This meant that a lot of changes needed to be made on our schematic. We realized that things were going to become a lot more complicated. We also weren't sure how to wire up the microcontroller chip as there were a lot of components we needed such as the boot up process and the clock, these were things that would have already been there in the development board. In order to figure out how to create the schematic, we looked at some previous examples of schematics from past years along with watching youtube videos online. In particular, we found one youtube video that created a dev board schematic using just a chip, so we were able to learn and apply a lot from that video to our usecase. It took a while, but eventually we were able to get the Schematic put together. At this point, we decided to work on the basic outline of the PCB so that we would have it in case we wanted to show it during the design review meeting. Since we already had the schematic done, it was just a matter of fitting everything within the 100 mm by 100 mm constraints and doing a lot of annoying trace wiring. After that was done too, we just did some preparation for the design review process to make sure that we knew how to articulate our ideas and that we weren't missing anything.
<br/><br/> 

![This is the updated schematic that we made](Schematic_New.png)

<br/><br/>

This is the updated schematic that we made.

<br/><br/> 

![This is the microcontroller section which is now based around just the chip](Microcontroller_New.png)

<br/><br/>

This is the microcontroller section which is now based around just the chip.
<br/><br/> 


## 03/05/2023 - 03/06/2023

We went to the PCB Review workshop a few days ago and got some feedback on our PCB from Professor Gruev in regards to our power subsystem and our trace widths all being one length. For our power subsystem, he recommendeded that for our voltage regulators, we regulate from 9V to 6V and 9V to 5V like we were already doing, but for 3.3V he told us to regulate from 6V instead of 9V. So we fixed that and then for the trace widths, we increased a lot of them from 10 mils to 20 mils. We tried to do it in particular for the places that will have high voltages or current going through the wires. After we fixed the PCB, we ran the DRC check and we ended up having a lot of violations due to our trace lengths and everything being too close together so we had to fix that. Next we put our Gerber and Drill files for the PCB through the PCBWay audit, at first it didn't pass but after we fixed a few things it passed. We plan to send in the necessary files for ordering the PCB tomorrow. Now that we had the schematic and PCB done, we decided to start ordering many of the parts that we had listed on the design document. Once these parts come in, we plan to begin testing the project so that we will be able to get everything working on time. After that, I completed the Team Evaluation which was straightforward and didn't take too long at all. 

<br/><br/> 

![This is the 3D view of our PCB](PCB.jpg)

<br/><br/>

This is the 3D view of our PCB.
<br/><br/> 


## 03/09/2023

A few days ago, Gregg emailed us that Dave would be the one in charge of completing the mechanical components for the project. So today morning, we went into the Machine Shop to give more details to Dave and make sure that he understands what we need. It was pretty straightforward, and he seemed like he had a good idea of what to do. I'm very excited to see how the finished product looks.
<br/><br/> 


## 03/20/2023

When we had the design review for the design document, we got some critiques on our document from our professor and TA. Most of these critques were in regards to certain things in the document not being specific enough. In particular some of the high level requirements and the R&V tables. Since the design document revision is due really soon, we decided to fix it so it was out of the way. We added some extra specifics to the high level requirements section along with some extra overhead costs to the cost and schedule section. As for the R&V tables, we rewrote the requirements section for the tables so that the requirements are more specific. The requirements now contain more details and exact numbers and features as opposed to being generic. We also slightly changed some of the verifications that we are doing for these requirements. 
<br/><br/> 


## 03/22/2023

Today we picked up our completed machine from the machine shop, and it looks really good. I'm excited to start working with it, and seeing the full product come together in the next month.

<br/><br/> 

![This is how the finished machine looks](Finished_Machine.png)

<br/><br/> 

This is how the finished machine looks.
<br/><br/> 


## 03/25/2023 - 03/27/2023

I finished a majority of the Individual Progress Report for the last few days. My teammates and I have worked on the entirety of the project together so far, so we had to figure out how to split up the subsystems so we would be able to write about separate sections. I decided to take the power and user interface subsystems as I was pretty comfortable with those two as I have done a lot of research on them. In terms of what I actually had to write, it did take a while because I was trying to make sure I didn't miss anything plus I was writing about two separate subsystems. For the power subsystem, the main thing that I focused on was the calculations for the resistances of the resistors that are used in the voltage regulator circuit. The equation for it is Vout = 1.25V (1 + R2/R1) + IAdj*R2, but IAdj is negligble, so the equation is actually Vout = 1.25V (1 + R2/R1). The R1 value is usually fixed at 240 Ohms in the circuits so this makes the equation Vout = 1.25 (1 + R2/240), and using this you can plug in your desired output voltage in order to get the resistance value for R2. As for the user interface subsystem, the circuits are fairly simple. Outside of these things, I had to write some verification for these subsystems which took a while to do but helped me understand the subsystems a little better. I also needed to write a schedule for what I need to do in the next few weeks, I realized that I am currently on schedule, but I will need to do a lot of testing in the coming weeks or the project most likely won't be done on time. 
<br/><br/> 


## 03/28/2023 - 03/29/2023
Today we picked up all of the parts we had ordered along with our PCB from ECEB, and began looking to make sure all our parts worked. We found out that some of our parts didn't work with the current PCB board that we had. However, it was mostly due to the parts we ordered being way too small, so we decided to reorder bigger sized parts as opposed to redesigning the PCB. We ended up ordering a lot more fuses, resistors, capacitors, connectors, and a few other parts. We also realized that we wanted a stencil as that will make it easier for us to solder all of the parts, so we sent in another PCB order but with the stencil as well this time. I also finished and submitted my Individual Progress Report.
<br/><br/> 


## 04/02/2023

Today, Adam and I began working on basic motor testing. Since everything was already built by the machine shop, we decided to test the motors directly on the project itself. The first thing we had to do was strip the wires so we could actually test with them. Next, we added a battery connector to our 9V battery so that it would be easier to connect to our breadboard. Since this was basically our first testing, we just began by testing that the DC motors actually worked when giving a voltage source. We tested the two DC motors that are on the shuffler along with the other DC motor which is used to launch the cards. When we attached the motors to the power, everything worked as expected and they rotated, though one of the motors was rotating the opposite direction of what we wanted, but we flipped the polarities and that fixed it. We then tried to see if the motors could actually shuffle the cards into the box. We applied the 9V source to our two shuffling motors, and it was able to shuffle the cards into the box, though the motor would stall at some points due to there not being enough voltage. We are considering increasing our voltage source to accomodate this, but we plan on looking into it more first. Next we tested the dealing motor, since this was only a single motor, we were able to get it working without any stall since we were using the 9V battery. So when Dave built the device for us, instead of creating a slit for us, he attached a metal bar to the device, and that metal bar is attached to some wirelike material, image shown below. This wirelike material is just a little bit too long, so the cards aren't able to be launched through it. We plan on going to the machine shop to get it sanded down a little bit more in the coming days so that it is short enough to let a card get through comfortably. For right now, we just used tape to create the size of slit that we wanted, image of this is shown below as well. We basically but a piece of tape where the card is launched into, and kept increasing the tape until it was able to only allow 1 card to leave at a time. Since we haven't written any code, to test we just plugged the battery in and out qiickly to see if the card could be dealt. It worked semi succesfully with 2 cards usually coming out at a single time. That's basically all the testing that we did today, hoping to start doing some more serious testing along with coming up with microcontroller code in the coming days.
<br/><br/> 

![This is the steel bar and wirelike material that will act as a slit for our final product](Card_Slit.png)

<br/><br/>

This is the steel bar and wirelike material that will act as a slit for our final product.

<br/><br/> 

![This is a picture showing how we used the tape to create a temporary slit for the card to be launched out of](Tape_Slit.png)

<br/><br/>

This is a picture showing how we used the tape to create a temporary slit for the card to be launched out of.
<br/><br/> 


## 04/04/2023 - 04/05/2023

Today, Adam and I worked on a variety of things, we tested shuffling, dealing, rotating the base, motor drivers, voltage regulators, and we wrote a decent amount of code to actually get some of our components working. When we initially ordered parts, we had thought that we could use a microcontroller development board, so we happened to have one that has the same chip as the one that we ordered, so we are currently using that to test everything. We started first with testing the voltage regulator to see if we could regulate down from 9V to 6V with the voltage regulator circuit that we had set up in the design document. At first it didn't work, but eventually we realized it was because we set up the voltage regulator incorrectly, with a few wires switched. Eventually we were able to get it working, and then we worked on setting up the motor driver to be able to power a single motor for the shuffler. After we had it all plugged in, we had to write some microcontroller code using CubeIDE for the STM32, the main portion of the code for this was pulse a PWM wave to the proper pins of the motor driver. It took us a while to figure out how to do this, as we had to set up a clock channel for this, and we also needed to learn how the IDE and microcontroller worked. We were able to get everything working correctly and power the single motor, we had to do some experimentation with different duty cycles, we didn't need a very high duty cycle since we were only using one motor. The problem came when we tried to power both motors, as it would spin for a little bit and then slow down and eventually stop spinning. We were able to determine later that it was because the voltage wasn't high enough. This is when we decided to connect two 9V batteries in series to create a 18V source, then we tested our voltage regulator again but with the 18V source. It took a while but figured out that regulating it down to 12V+ would allow for constant stable motor rotation. We adjusted our voltage regulator circuit to account for this, we also had to make these modifications on the PCB and schematic. At this point, we continued adjusting the code to make sure the motors were spinning at a good speed, this would allow the cards to become shuffled uniformly. Now that we had the entire shuffling process working, we worked on the dealing mechanism. This motor was a similar set up to the shuffler ones since it's also a DC motor. We code it in a similar way at first, where we just had it rotating outwards, but then we realized that it wouldn't distribute one card at a time, often distributing like 5 cards. We realized the first reason was because we were still using the 12V regulated voltage, so we decreased it to our original plan of around 6V and that helped a little bit, though it still wasn't very accurate. This is when we decided that we were going to use the motor driver to help us in distributing only a single card. What we did was to rotate the motor forwards for a bit, and then rotate it backwards using the motor driver and the code that we wrote. We had to do a lot of trial and error, but eventually we were able to get it so that it is able to distribute only 1 card around 90 % of the time and occasionally it would 2 cards. Though not fully successful, this is a major improvement from what we had before. The last thing that I worked on today was figuring out how the servo motor connections worked. I learned that how to control this motor is that we pass in a PWM signal to one of the 3 pins, and the other 2 pins receive a ground and power signal. We passed in the 12V regulated voltage to this motor, as we thought it might be beneficial due to the amount of torque that the motor would need to rotate the entire device. Since we already had a good amount of experience with creating and editing the PWM signal through the STM32 CubeIDE, it was quite easy to control this Servo motor. We were able to write up some code that allows us to rotate the base of the device back and forth. Today was overall a pretty productive day and we made a lot of progress on the project. We also fixed the schematic and PCB to account for the extra voltage regulator that we are adding, we ran that on the PCBWay audit and it passed, so we sent it to our TA so that it can be ordered. We also ordered the extra voltage regulator that we needed, along with a few extra parts in case anything ends up not working.
<br/><br/> 

## 04/07/2023

Today, I spent the majority of the time just figuring out how to write the code to get the ultrasonic sensor working. The wiring of this sensor is pretty straightforward as there is only 4 connections, ground, vcc, trigger, and echo. Ground and VCC are the same as always, the Trigger pin is an output from the micrcontroller that will trigger the signal to start the sensor, the echo is the input into the microcontroller that reads the value from the ultrasonic sensor. I was mostly just following guides online in regards to the code, it was a lot easier to set everything up this time as opposed to last time since I know how the CubeIDE works. The code was also relatively straightforward. I was also able to figure out printf() statements so that it makes debugging easier through the microcontroller. With what I was able to do today, I have the ultrasonic sensor functioning such that it will print the distance in centimeters that the object is from the ultrasonic sensor. My next step is to figure out how printing to the 7-segment display works so that I can get started on the UI.
<br/><br/> 

## 04/14/2023

Our newest PCB order just came in, so we are going to start testing with that and soldering parts onto it. The first thing that we needed to do was to reconfigure the pin layout on the CubeIDE to fit our initial PCB design that we had in place. This is because we had been testing with the microcontroller development board and so the pin layouts weren't correct, we had chosen them randomly as we had been testing. We were able to reconfigure the pins fairly quickly and translate our code over to the PCB schematic as it was practically the same. 
<br/><br/> 

## 04/14/2023

Today we went to ECEB to begin soldering all of the parts to the board. We were recommended from our TA and others not to use the PCB oven as it might damage the parts and make them difficult to use so we decided to hand solder. We startexd by watching videos online in regards to how to solder SMD microcontroller chips and what the best methods are. We found out that it isn't too hard to apply a lot of flux and then just run the solder across the pins. So we decided that this is what we were gonna try, it ended up taking us like 6-8 hours just to solder on the Microcontroller chip. At one point though, we accidentally bridged a few of the pins, and we tried to use the soldering removing tool at ECEB, but we ended up bending a few of the pins. We tried to take the chip off of the PCB to bend them back into place, but we weren't able to do it properly so we decided to just use a new chip and PCB board. We then did it a lot more slowly, and after a few hours we were able to get the chip on without any bridging, it was late at this point though, so we decided that we are going to solder on the rest of the pieces tomorrow.
<br/><br/> 

![This is the PCB after we soldered on the Microcontroller chip.](PCB_Microcontroller.png)

<br/><br/>

This is the PCB after we soldered on the Microcontroller chip.

<br/><br/> 

## 04/15/2023

Today we went into ECEB, and basically soldered all of the components onto the PCB. We started with all of the resistors which took quite a while. After the resistors were on, we moved onto the fuses, however we realized we were missing a few fuses, but since fuses are used solely to protect the circuit, we just bridged the fuse terminals on the PCB so that our PCB would not be an open circuit. We assumed that should work since we just basically have the same circuit as before but without the fuse. We next attached the usb as that would be needed to program our microcontroller, this was a little difficult to put on because it had to be soldered underneath as well. In order to accomplish this, we used the solder paste and the hot air gun, which appeared to work. Next we attached the voltage regulators, so that we will be able to supply power to our Microcontroller chip so we can get a working project. Lastly, we attached all of our connectors and the crystal to the PCB. We did realize that we actually didn't need the crystal because our chip actually has an internal clock in it, but we still connected it since we already had the component. At this point, we had everything connected to our PCB, it had taken maybe like 6ish hours or so and at this point we were quite tired. However, we decided that it would be a good idea to begin doing some testing. We spent the next few hours testing, but we weren't able to get it working and weren't able to really get any readings from our PCB. We assumed this could be for a few reasons, our capacitor footprints were a little bit small which was problematic, we hadn't soldered properly since we don't have much experience with it, and we potentially had a lot of cold solders. At this point, we made an executive decision as a group that it might be a better idea to switch from the PCB to the microcontroller as we don't have a whole lot of time left to complete this project. If we keep trying to work on the PCB, there is a chance that we don't get anything working and have nothing to show for the final, the microcontroller on the other hand, we know some of the stuff can work since we have tested it before. 
<br/><br/> 

![Adam and I at the lab working on the PCB.](PCB_Soldered_Lab.png)

<br/><br/>

Adam and I at the lab working on the PCB.

<br/><br/> 

![This is the PCB after we soldered on all of the components, even though we aren't using it.](PCB_Soldered.png)

<br/><br/>

This is the PCB after we soldered on all of the components, even though we aren't using it.

<br/><br/> 

## 04/16/2023

We had disassembled our initial circuits that we had set up for shuffling and dealing, since we thought we were gonna use the PCB. Building the circuits again was just a matter of building out our schematics on a breadboard and then testing them to make sure that everything is working as expected. We started with the power subsystem as we needed it in order to test anything else. After we set up each voltage regulator, we tested them with our voltmeter to check that it was roughly the correct voltage, we didn't do more thorough tests since we don't have the load from the motors affecting it yet. We then adjusted the resistors for some of the regulators, if we weren't getting the voltage values that we wanted. Next, we set up the motor drivers and checked that our code was still able to run as we had it a few weeks prior, however our code wasn't working correctly. At this point, our batteries were starting to die adn the wires for our motor were a little bit short so we decided that we were going to go to ECEB. On the way to the ECEB we picked up 2 9V batteries, and when we got there we soldered on some more wires to the motors. This allowee plugging into the breadboard a little bit easier. We continued to try to get the code for running the shuffling work but for some reason, it wasn't working at all. After a little while, we switched to the power supply to see if it would provide a more regulated voltage than our batteries. An hour or so later, we ran into a huge error, our microcontroller development board stopped allowing us to program and stopped working as a whole. When we felt the components of the board, the chip was really really hot, and we figured out that we had fried the chip. At this point we were very stressed out as we didn't know what we could even do. I went ahead and ordered another microcontroller dev board but it doesn't come in until Thursday and our mock demo is on Wednesday. This is when I decided to make some calls and see if anyone else had an extra development board and it turned out that my roommate had an extra development board that was the same as ours. We headed home, and restarted our circuit because we were scared that it had been the reason our board fried. However, we later figured out it was most likely due to us using the voltage supply and setting the voltage output too high. We restarted with the voltage regulator circuit and motor driver circuits, and then plugged in the motors to the drive. We probed the voltage regulators without a load and they came to the expected values. We then fixed the code and now it somehow ran for the shuffling though we didn't make a whole lot of changes. It wasn't great at shuffling so we did some testing and edited parameters until it worked as we expected it to and we got it to where it has almost an even shuffle with occasional sections of two cards being pushed in instead of one. We then fixed the dealing as well and it worked fairly well, distributing 1-2 cards per iteration. At this point, we tested the voltage outputs of the regulators with a load, and modified the resistors as necessary. The outputs that we got from these regulators has been shown in the images below. At this point, we were pretty satisfied with our progress since just a few hours prior we had fried out microcontroller dev board and had nothing working, now we had shuffling and dealing working. Our goal for tomorrow is to learn how the buttons work, integrate shuffling and dealing together, and figure out how the display code works for the LCD.
<br/><br/> 

![This is how the circuit for the power and motor subsystems looks on the breadboard.](Breadboard_Schematic.png)

<br/><br/>

This is how the circuit for the power and motor subsystems looks on the breadboard.

<br/><br/> 

![This is the output from the 12V voltage regulator.](12V_Output.png)

<br/><br/>

This is the output from the 12V voltage regulator.

<br/><br/> 

![This is the output from the 6V voltage regulator.](6V_Output.png)

<br/><br/>

This is the output from the 6V voltage regulator.

<br/><br/> 

![This is the output from the 5V voltage regulator.](5V_Output.png)

<br/><br/>

This is the output from the 5V voltage regulator.

<br/><br/> 

![This is the output from the 3.3V voltage regulator.](3.3V_Output.png)

<br/><br/>

This is the output from the 3.3V voltage regulator.

<br/><br/> 

![This is how the circuit for the power and motor subsystems looks on the breadboard once connected to the microcontroller.](Breadboard_Schematic_Microcontroller.png)

<br/><br/>

This is how the circuit for the power and motor subsystems looks on the breadboard once connected to the microcontroller.

<br/><br/> 

![This is a video of how the dealing works right now.](Dealing.MOV)

<br/><br/>

This is a video of how the dealing works right now.

<br/><br/> 

## 04/17/2023

Today was a really long day in terms of how long we worked on the project. Firstly, I started my day by going to ECEB and making the wires for the motors even longer because I know that when we start rotating using the servo motor we will need really long wires because otherwise the wries will come off of the breadboard. After I got back, the first thing that we did was the make sure that the shuffling and dealing was working properly. Then Adam decided to put the schematic on Fritzing, and while this was happening, I worked on understanding the buttons and the LCD display so that we could get things working in parallel. I understood the buttons as they were quite straightforward, but I used a different breadboard to get the LCD Display working, but it wasn't working with the intial code that we used. We did some more research online and eventually found some code online that allowed us to write properly to the display. At this point, we knew how the buttons and LCD display worked, we had shuffling and dealing working, and Adam had code that would allow for servo rotation, and I had code for the Ultrasonic sensor. At this point, we decided that it was time to start integrating everything together. We had two breadboards, one for power and motor subsystems, and another for the sensing and user interface subsystems, we decided to attach both of them to the microcntroller dev board as necessary at this point. Then we began integrating everything together. The first thing that was done was to allow a button press to control the number of players and the game mode that we were playing. At first it wasn't working because each time we pressed the button, it was incrementing our counters too much. Eventually it worked though since we ended up adding a delay, which was basically equivalent to debouncing our circuit. I then added a button in the code that would allow for shuffling and lastly for dealing. At this point, there was code that when the button was pressed, it could increment the number of players, increment the game mode, start shuffling, and start dealing cards one at a time in front of the device. Next thing that was done to output all of the necessary information to the LCD display, so when we start the device, it will start off with a welcome message. When we are incrementing number of players, it will say select number of players and then list the current number of players as you press the button, it's a similar process for the game mode. For shuffling, it says on the display, that shuffling is currently occurring, and same for the dealing process. At this point, I decided to create a very basic version of the poker game, so they would select a minimum of two players, game mode 1, and then press shuffle which will shuffle the 2 decks of cards, and then they press deal once which will deal 2 * the number of players in cards. So each player now has 2 cards in hand, if you press deal again, it deals 3 cards which is for the river. Next time they press deal, there is another card that will be dealt for the 4th card in the river. The last deal will allow for the last card to be added to the river, which is all of the dealing that is necessary for poker. As far as progress goes, I was very happy with what we did today because we didn't have very much working just yesterday. As far as what we have left, we need to get the servo working and intergrated along with the ultrasonic sensor, and then we need to make small edits and changes to remainder of our project to make it better and more user friendly. Next up, we have our mock demo tomorrow, I think we should be fine for that, though our batteries are a little bit dead, but we ordered some more which should come in on Thursday.
<br/><br/> 

![Current circuit with user interface, power, and motor subsystems.](Circuit_Demo.png)

<br/><br/>

Current circuit with user interface, power, and motor subsystems.

<br/><br/> 

![This is how the LCD display looks at boot up.](LCD.png)

<br/><br/>

This is how the LCD display looks at boot up.

<br/><br/> 

![Video showing the current progress for the project.](Mock_Demo.mov)

<br/><br/>

Video showing the current progress for the project.

<br/><br/> 

## 04/20/2023

Today, we decided to take a break from the project, and just work on the team fullfillment contract and plan out when and what we will work on in the coming few days. The team fullfillment contract was pretty straightforward and just built on the team contract that we did in the past. It only took us like an hour or two to complete and also wasn't hard as we didn't have a lot of issues among our team.
<br/><br/> 

## 04/23/2023

At this point, we had the LCD display integrated with the shuffling, dealing, and buttons. The code that we had written for this was very basic and just dealt cards in front of the device since we didn't have the ultrasonic sensor or the servo integrated into the code, so that was our goal for today along with making fixes to whatever we had working already. I had the ultrasonic working already previously but it needed to be integrated into the code. Adam has the servo working but when we tested that it was with a mini servo not the one that was on the device, so Adam made some edits in order to fix that. We found out that the 360 degree servo that we bought isn't the best for the prevision control so we couldn't get exact measurements, so manual trial and error was what we had to do. At this point, we thought it would be best if we could integrate the servo with the ultrasonic sensor prior to integrating it into the rest of the code because it would be easier to do this. So the goal with this was to slowly rotate the servo 180 degrees, but if the ultrasonic sensor detects anything at any point, it should stop for a bit and then continue rotating after, this was what we were trying to set up for testing. However, it wasn't working very well when we set it up, so we began to debug it. The first error that we found was that the timer channel that we were using for the ultrasonic sensor wasn't set up correctly. The reason that we need a timer channel is because the way the distance is calculated is by taking the time difference between the rising and falling edge of the signals from the ultrasonic which requires a timer. After we fixed this, it started detecting things, but now it was detecing things when nothing was there so we were trying to use printf but for some reason, it was printing weird characters such as question marks, and after about an hour of trying to figure it out, we determined it would be better to do it another way. Then we decided it might be better to print it out through using the LCD display instead. So at this point, we started using the LCD display to print the distances, and we realized there was an error with the code, in particular the distance variable needed to be reset after a dealing was done. After this was fixed, it would stop when detecting objects which is what we wanted. Now we wanted to integrate everything together into the same code, I worked on this portion of the work. While I was doing this, Adam was mounting the breadboards and microcontroller onto our machine so everything was in one place, along with rewiring as necessary to make everything work. Eventually I got the code all put together, and Adam got the project wiring set up. The code structure that I set up for today was mostly just for poker and goes as follows: press button 1 to increment the number of players, press button 2 to change the game mode (change it to poker), press shuffle and it should shuffle at this point, and lastly is deal which works different from it did before. Now the deal has servo and ultrasonic integration in it, so when you press deal the first time for poker, it will rotate around until it sees a player and it will stop there and deal 2 cards and continue doing that until it reaches approximately 180 degrees, and then it will rotate roughly back to the starting. That is what the code should do in theory, but there was some bugs. One of the bugs had to do with the fact that after the dealing was done, the ultrasonic sensor would pick up the same person again and it would deal again, to combat this we just decided to rotate a few times right after a dealing in order to get past the current person onto the next one. Also the display was acting weird and printing interesting characters to the screen, but that might have been from a loose connection, as when we plugged it in again it seemed fine. There were a few other minor bugs, but we fixed them. Currently our dealing mechanism needs some work because it deals a few more cards then we want it to. That was in essence what we did today. Tomorrow is the last day we have to work before our demo, so the plan is to fix the dealing mechanism, add in all of the necessary games, clean up and comment out the code as needed, make small fixes and edits where it might be necessary, prepare the R&V tables with our testing and the data that we need to show for the demo, and lastly just prepare for the demo. As of right now we have done a lot of testing, but not necessarily of the requirements that are in the tables, so we are going to have to do a lot of testing tomorrow. Overall, I do think we are in good shape for the demo, not as good as I would've wanted at the beginning of the semester, but we put in as much work as we possibly could put in and hope that will end up being enough.
<br/><br/> 

![Current device with all of the parts mounted.](Final_Device.png)

<br/><br/>

Current device with all of the parts mounted.

<br/><br/> 

![Video showing the progress with everything mounted.](Demo_Mounted.mov)

<br/><br/>

Video showing the progress with everything mounted.

<br/><br/> 

## 04/24/2023

Today was the very last day before our Demo tommorrow at 5 PM. I think with what we had prior today, we would be okay, but we definitely wouldn't of gotten the grade that we wanted. I think all of the additions that we made were good and will help us demonstrate the functionality of the project better. The first thing that I did was to modularize and make the code a lot cleaner so that it will be easier to add more functionality and game modes. Basically the code before was very redundant, we had 3 game modes at this point, one that would deal cards straight in front of you as a pile, one for dealing an even amount of cards to all players, and one for poker. These game modes weren't fully set up or fully functional at this point, but the basic code was written, and with a little bit of debugging it should work in theory. For all of the current functions, the redundant code includes dealing, rotating clockwise, rotating counterclockwise, and calculating the distance from the ultrasonic sensor. So I basically made a function that shuffles, rotates clockwise/counterclockwise once, dealing once, and calculating distance once. Most of these functions are passed in a variable called iterations, and this is used to determine how many times the operation should be performed. After I created these functions, I started replacing them in the already written sections of code and checking if everything works as expected, eventually I was able to replace all of the redundant code with these functions. At this point, now that the code was cleaned up, I added a lot more comments to the code to make sure it is more readable. At this point, I added code for two more game modes using a similar format to what I had done previously, the game modes were Rummy and Uno. I wrote the code fairly quickly now that the functions were abstracted and easier to use and less copy pasting. Now that the basics were done for each game mode, we began to work on the Requirements and Verifications tables which took quite a while. We had to make some edits to the tables since some of our requirements and verifications tables were no longer accurate from our design document. Also we had to add in all of our testing data that we had collected throughout the semester as needed for the verification sections. I will attach the images of the tables for each subsystem and all the testing that has been done to the end of this entry. Any of the requirements that don't have a results section indicate that we will be showing them the verification during the demo. All of these requirements had been tested manually by the end of the night to make sure that they could be shown tomorrow. After we had completed and updated the requirements and verifications tables, we went to Grainger to print out all of the documents we would need for the demo including the R&V tables, the high level requirements, and the block diagram. Once we got back home, we began working on fixing the dealing system. The dealing wasn't work as great as we would have hoped for, initially we had something made from the machine shop that we were going to get sanded down to work well. However, it wasn't possible to do this anymore since we were having a breadboard now covering the front area of the device. We tried a few different materials, we first tried a cardboard piece, we tried tape by itself, we tried a section of a thin cardboard box that held fig bars, and lastly we used paper. We ended up sticking with paper because it worked the best out of all of the options we had to test with. We were getting mostly good deals after using the paper and adjusting the code as needed. Most of our deals were giving out 1 card, occasionally the card would get stuck to something on the device, also the motor has a slight tilt one way so the cards sometimes would go that way and get stuck. However, we figured that it was good enough to demo for tomorrow since we didn't have enough time to find a better solution, because at this point it was already close to 12 AM. At this point, we worked on fixing the rotation a little bit as it isn't super accurate with the 360 degree motor that we have for the rotation of our device. We were able to rotate to close to 180 degrees forward and backward after a while of testing and modifying the duty cycles of the motors along with the number of rotations that were being done. At this point, we were close to wrapping things up, so we began testing the game modes that we had. We started with the first one that deals all the cards in front of you, this one worked fairly well without many issues. The next game mode was an even split of the cards to each of the players. Here we had a small coding issue to fix, basically if we set two players for example, our device would deal to even a 3rd player if they saw that this player through the ultrasonic sensor. We had to modify the code to make sure that it is only dealing to the specified number of players. Once we fixed this, everything worked as expected with this game mode. The next 3 game modes that we tested were Poker, Rummy, and Uno, all of these game modes worked as we had wanted to, however we still had some weird things going on with the rotation of the motors, so we investigated that. We just modified the duty cycles a little bit and the number of rotations that we were doing and the length of the rotations in order to get the forward and backwards closer to 180 degrees each. At this point basically everything had been tested. The last thing that we did tonight was to prepare for the demo a little bit and come up with a good gameplan. The only things we would have to do tomorrow are to replace our current batteries and tape up these new batteries to the device so that they wouldn't be hanging out, we would also have to make sure that everything worked as expected with these new batteries. 
<br/><br/>

Below you can see the entire R&V table for this project that we have put together, including testing/changes where necessary.

<br/><br/>

![Picture 1](R&V1.png)

<br/><br/>

![Picture 2](R&V2.png)

<br/><br/>

![Picture 3](R&V3.png)

<br/><br/>

![Picture 4](R&V4.png)

<br/><br/>

![Picture 5](R&V5.png)

<br/><br/>

![Picture 6](R&V6.png)


<br/><br/>

![Picture 7](R&V7.png)

<br/><br/>

![Picture 8](R&V8.png)

<br/><br/>

![Picture 9](R&V9.png)
<br/><br/>

![Picture 10](R&V10.png)

<br/><br/>

![Picture 11](R&V11.png)

<br/><br/>

![Picture 12](R&V12.png)

<br/><br/>

![Picture 13](R&V13.png)

<br/><br/>

![Picture 14](R&V14.png)

<br/><br/>

![Picture 15](R&V15.png)

<br/><br/>

![Picture 16](R&V16.png)

<br/><br/>

![Picture 17](R&V17.png)

<br/><br/> 

![Video showing our team using the device to shuffle and deal for Poker.](Poker_Demo.mov)

<br/><br/>

Video showing our team using the device to shuffle and deal for Poker.

<br/><br/> 

## 04/26/2023

We had our demo today at 5 PM, we arrive to ECEB around 3:30 PM to prepare for the demo. We bought all of the equipment that we needed for the demo, the hardest thing to trasport was definitely the actual device as its very bulky but also super fragile, so even having one wire get pulled out could make it hard to get it working. Fortunately Adam made a Fritzing schematic so if that had happened it probably would've been okay. After we got to ECEB, we replaced our batteries with new ones and tested the Poker game mode to make sure it was still working as expected. We then went to our Demo, and everything appeared to go well, some of the things didn't go as well as we expected them to based off of the results that we had while testing. However, it seemed that everyone enjoyed the demo and we feel like we had a pretty fun and interesting project to show off so I was overall happy with the results of our Demo. At this point all that is remaining for this semester is the peer demo, mock presentation, peer presentation, final presentation, extra credit video, final paper, and this lab notebook, so the bulk of the work has been completed. Also Ralph has begun working on the presentation and final paper already, so that should help reduce the load for when we work on it as a group. 
<br/><br/> 

## 04/27/2023

Today, we worked on our final presentation. We just followed the rubric and the things that were on the website in order to figure out how our presentation should be organized and what should actually be put on it. It took like around 2 hours to make the presentation and put in the pictures and the information, but it was mostly just writing down all the knowledge that we already knew so it was actually pretty straightforward. The current presentation that we have set up today is just a draft, we will probably end up making a decent amount of edits after our mock demo tomorrow as needed. We also filmed the 1 minute long extra credit video today so that we could put it in our presentation as a demo along with submitting it for extra credit. I'll attach the video to this journal entry.
<br/><br/> 

![Video that we filmed for extra credit of demo.](EC_Demo.mov)

<br/><br/>

Video that we filmed for extra credit of demo.

<br/><br/> 

## 04/28/2023

Today, we had our mock demo at 8 AM, it was quite hard to wake up that early so I was definitely pretty tired while presenting so my energy wasn't the same as it hopefully is during the actual presentation. In terms of our actual slides, some of them need to have less actual information on them, we need to add a few more pictures, and we need to talk about all of the cool functionality that our project actually has. We also were told by our TA, that it might be a good idea to bring in the actual device to explain it as we go through the presentation. Another thing that we were told is to split the slides more evenly among the group because some people ended up talking longer than others. In terms of my presentation skills, I was told that I was swaying around too much and sometimes putting my arms up in my sleeves which I had no idea that i was doing, so it was good to learn those things. I will definitely keep these in mind for when I am actually presenting on Monday.
<br/><br/> 

## 04/30/2023

Today we worked on cleaning up the presentation a little bit, making it easier to read and nicer to look at. After that we practiced our slides for a while and also split up the slides among us so that we each had an even amount of speaking time. After we had finished practicing our presentation, we decided that we wanted to work on the final paper so we worked on it for around 4-5 hours, and we made a lot of progress. All of the information is basically on the slides, but we need to do a few hours of editing tomorrow after our presentation at 5 PM. After the final paper has been completed tomorrow night, the class would basically be done for me, so I am looking forward to that.

## 05/01/2023

We had the presentation today, I felt pretty prepared going into it as I feel like I know exactly what is going on in the project and know what is going on in every portion of the project. However, I thought that the presentation went okay, solely because we ran out of time and our demo video didn't work, I think I would've felt better about it had those two things not happened. After that, we just worked on our paper the rest of the night, we made lots of progress. Basically all that is left is to fix the references section of the paper and reformat it a little bit to look nicer. 

## 05/02/2023

Today is basically the wrap up day for this class. I had my peer presentation earlier in the day, after that I began working on the paper again, all we had left was references basically. We finished that, reformatted the paper as necessary and made it look nice, and submitted it to PACE. Right now I am writing this last lab notebook entry, and then I am planning on uploading everything to Github. I have also decided to include all of the Documents that we have done so far in this class including Project Proposal, Design Document, Individual Progress Report, Final Paper, and the Final Presentation. All of these documents will be included in this folder for anyone who might read this notebook. I also thought that it would be good to include all of the references that we have used as a group at the end of this entry. 

![Final References.](Final_References.png)

<br/><br/>

Final References.

<br/><br/> 

## Reflection

As a whole this project has been very fulfilling, and I feel that I have learned a lot about teamwork, designing something from start to finish, presentation skills, and also various technical skills such as embedded programming and soldering. However, I am glad that this project is finished as I am looking forward to graduating.