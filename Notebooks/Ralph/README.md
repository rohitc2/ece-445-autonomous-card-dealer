# Ralph Worklog

[[_TOC_]]

# 1/17/2023
## Initial Post
Submitted initial post onto the UIUC ECE 445 WebBoard as a project proposal. 

```
CONCEPT
- We all love card games. To name a few: Poker, Literature, Blackjack, Kings Corner, etc. We all want a fair card distribution system which guarantees the 'dealer' is not cheating.

USAGE:
- A shuffled deck of cards will be placed in a holder.

- The automatic card dealer may have several modes:
    - distributes cards evenly amongst players for games like Literature and Kings Corner
    - "poker mode" deals flop, turn, river ?
    - "blackjack mode" deals blackjack ?

- Once the cards have been depleted or the hands have been dealt, the automatic card dealer should ideally stop dealing the cards and enter a sleep mode.

IMPLEMENTATION:
- As far as actuators/motors, I believe we would need 2. One to control rotation of the trajectory (rotate or move the deck) and one (maybe two) to "pick up the card and throw it".

- As far as sensors, I believe we would need 1 to determine if there are any more cards to deal.

- Let's say there were 3 buttons:
    - Mode,
    - Number of Players,
    - On/Off

- The state machine will always start running to the "next location". When someone presses the button, our machine should stop at the following locations. Assuming +degrees is to the left of the dealer.

- (2player) 0,180, always deal first card to 180 deg position first
- (4player) 0,90,180,270 always deal first card to 90 deg first
- (6player) 0,60,120,180,240,300 always deal first card to 60 deg first
- (8player) 0,45,..., always deal first card to 45 deg first
Is that something we can do?
```

# 1/20/2023
## 445 Staff Feedback & Team finding
TA, Dushyant Singh Udawat, and Professor Fliflet commented on the Web Board post showing approval for the project as long as we figure out the exact scop of the project and decide to pursue the exact problem. We will also consider the card counting and card dealing implementation. 

In addition, I was able to talk to two classmates: Adam Naboulsi and Rohit Chalamala. We discussed several possibilities for projects. The wall painting robot, something related to robotics, computer vision, camera gimbal, etc. We always have the Card Dealer and Shuffler as an option. Am open for more computer vision based projects.  

# 1/23/2023
## Laboratory Safety Training and Brainstorming
Submitted the Laboratory Safety Training on Canvas. Our group met up today to continue brainstorming ideas for the class. The Autonomous Card Dealer is an option and we are still interested in pursuing the project, but are open to other computer vision based projects. 

# 1/25/2023
## Worked on Request for Approval RFA
We worked on writing up the RFA for the Autonomous Card Dealer and Shuffler. We worked on the proposal, including the Problem, Solution, Solution Components, and Criterion for Success for the Autonomous Card Dealer and Shuffler.

Our team finished working on the Request for Approval. We were able establish the problem we wanted to solve and the components necessary to produce a solution. We decided to pursue the Autonomous Card Dealer project, but are open to changing if necessary. We established that there involves 3 main components: Shuffler, Dealer / Distributor, and User Interface. In the RFA, we discussed each of the components's purpose and usage, and their cooresponding subcomponents.

# 1/26/2023
## Early Request for Approval and New Project Idea
Early Project Approval, submitted early Request for Approval for the Autonomous Card Dealer. We worked on our CAD, PCB, mini-encabulator assignment.

Spoke with TA Wang Yixuan about a project in computer vision and robotics: use computer vision (via a Logitech Webcam) to create a lane following RC car. I was thinking of using the BeagleBone Black to process live video footage. And will run programs to alter the steering angle (angular position) and throttle of the RC. 
Feedback: the budget should be considered. Processing speed. 


# 1/27/2023 
## CAD Assignment
Submitted the following documents:
![rbalita2_miniencabulator.kicad_pcb] (documents/rbalita2_miniencabulator.kicad_pcb)
![rbalita2_miniencabulator.kicad_sch] (documents/rbalita2_miniencabulator.kicad_sch)

Shoutout to Adam and Rohit for their help on the schematic and PCB. They helped me with some confusion that I had with component associations, unrouted nets, and GND stitching
Feedback: Incorrect Dimension, No copper fill in front and back layer


# 2/3/2023
## Soldering Assignment
Demoed my Mini Encabulator Soldering Assignment. All test cases passed. Shoutout to the Lab TA who was able to help me debug my curcuit and introduce me to using the oscilloscopes and voltmeters for each individual comonent right before my demo. 

# 2/07/2023
## Team Meeting #0
Our team met with Nikhil Arora to discuss course logistics, the timeline of the project, the project idea, how to split up the work on the subsystems. "Take over the casino industry". In addition, our group did not end up choosing to utilize the combo or locker for the project. 

# 2/08/2023
## Project Proposal for the Card Dealer Initial
We started working on the Project Proposal. We ended up recycling and updating some of the material from the RFA, including the problem, solution, Subsystem Overview, etc. We established the three main high level requirements for the Autonomous Card Dealer. I began writing some high level requirements, and ways to determine a successful test and a failed test. The basic functionality of the device are the high level requirements. 

```
### Shuffle a set of cards evenly
- Given any two sub-decks, the shuffling system shall perform one riffle shuffle. The result of the riffle shuffle shall be a deck of cards that are aligned and ready to be distributed. No card left behind. 
- One test that our group plans to perform and test the riffle shuffle mechanism involves two sub-decks. i.e. 13 in-ascending-order cards Spades suited and 13 in-ascending-order cards Hearts suited. The test will be noted as a SUCCESS if the result of the shuffling mechanism is shuffled any order of As-Ah-2s-2h-3s-3h-4s-4h-5s-5h-6s-6h-7s-7h -8s-8h-9s-9h-Ts-Th-Js-J-Qs-Qh-Ks-Kh. The test will be noted as a FAILURE if the resulting deck does not include all of the cards (some cards were left behind). We will analyze the effective-ness of the mechanism by comparing the order of the sub-decks before  and the resulting pile of cards after the card shuffle is done .

### Distribute the cards to the players and Set up playing field
- Given an aligned and ready to be distributed deck of cards, the distribution system shall perform a dealing mechanism. The dealing mechanism shall pick up the cards one at a time and deal the card towards its intended trajectory. This ‘dealing’ action is performed until the deck has been depleted. 
- The first test that our group plans to perform in order to test the card distribution system involves one deck of unshuffled cards and preset modes. i.e. number of players = 2, and game mode = evenly distributed amongst all players. The test will be noted as a SUCCESS if the number of cards dealt to each player is the same (26 each). The test will be noted as a FAILURE if not all cards are dealt OR if the number of cards dealt to each player is NOT the same. The distribution system shall rotate to a preset ‘next’ angular position, stop rotating, and deal a card. The system shall continue to perform this act for all preset ‘next’ angular positions (0,90,180,270) until the deck is depleted. Our group plans on generating more tests that involve various number of players (4,8) which are restricted by the game mode. Overall, Tthis system can be tested visually by checking that the cards that are on the board have been distributed correctly and in the right order.

### A working  user interface
- Our goal is to abstract the device to work on multiple game modes and various numbers of players. The user interface shall involve 4 buttons. On/Off, Start/Pause, toggle number of players, and toggle game mode. This high level requirement is software heavy, given that the various game modes restrict the number of players. This, in turn, will impact how the deck is dealt. The test will be noted as a SUCCESS if the device deals the correct piles of cards to the correct number of players for a given game mode and num_players. The test will be noted as a FAILURE if the dealing sequence fails to match the request of the user. Test each of the buttons to make sure they are performing as expected.
```

Adam and Rohit did an amazing job with working on the block diagram and physical design. Very clean and very detailed. Here is the Physical Design that Rohit uploaded to the Design Document. This is the original Physical Design. 

![](images/phys_design.png)

![](images/block_diagram.png)

# 2/9/2023 
## Project Proposal - Initial Submission
Submitted the initial Project Proposal for the Autonomous Card Dealer. I focused on the Introduction and some of the Design sections, working on the high level requirements of the system.

# 2/13/2023
## Machine Shop Meeting #0
Spoke with the Machine Shop today to talk about how we should go about the physical and mechanical design of the project. We discussed the feasilibity of the projects mechanical design and the approaches we can take to successfully create a working shuffling and dealing mechanism. They proposed we utilize a rotating base from a previous project, and pair it with an off the shelf card shuffler. We also discussed the possibility for us to use 3D printed parts for the PCB encolsure and card holder

# 2/14/2023
## Team Meeting #1
Discussed Schematic and PCB design. We discussed the progress of the project proposal for the Autonomous Card Dealer. Discussed the feasibility of the project, the features/goals, kinds of materials that we will need. In particular, we focused on the tolerance analysis risks and their cooresponsding tests. We discussed the dealing mechanism, the dealing motor, card slit dimensions, and card launching mechanism. In addition, we also took into account the measurements for the ultrasonic sensor. 


# 2/20/2023 
## Design Document Update
We began working on the Design Document. We recycled and updated the contents from the RFA and Project Proposal: introduction (problem and solution), high level requirements. We included a few requirements for the power, user interface sensing, and motor subsystems, including power requirements, communication protocals and motor capabilities

We also finalized the schematic and PCB.

![pcb_design](images/pcb_design.png)

## Team Contract Write up
We worked on the Expectations (ground rules), Roles of each member, Project Meeting Times, Agenda, Process and Penalties for dealing with team issues

# 2/21/2023
## Team Meeting #2
Discussed with Nikhil our mechanical design, the PCB design, and the progress we made on the design document. We also went to the Machine Shop to discuss the progress of our design. We decided on buying an off-the-shelf card shuffler. We would control the motors from the microcontroller code, but use the enclosure and motors of the card shuffler.

# 2/22/2023
## Design Document Update
Added the cost and scheduling section for the design document. At the moment, our bill of materials only includes the main comonents (no resistors, capacitors, and connection components) like the microcontroller, motors/motor drivers, 7 segment display, voltage regulators, and card shuffler.  In addition to the cost of the main components/materials, we must also account for the average pay of a new grad - cost of labor for the project.

# 2/23/2023 
## Design Document - Initial Submission
Finalized the Design Document today. Rohit included a really good visual aid and physical design for readers to get a better understanding of how our device works. Adam and Rohit worked on making our first draft for the circuit schematic of the device. Adam showcased each of the subsystems and their cooresponding schematics and added more requirements and verifications on each of the subsystems. Helped out in adding a few verification methods for the Motor Subsystem and user interface. Finalize the Bill of Materials by including the rest of the components on the schematics and their cooresponding links. Finished the Estimated Hours of Development, External Materials/Resources, and Schedule. Submitted the initial Design Document for the Autonomous Card Dealer.

# 2/24/2023
## Team Contract
Our team submitted the Team Contract for the Autonomous Card Dealer. 

# 2/26/2023 
## Project Proposal - Second Submission
Submitted the second version of the Project Proposal for the Autonomous Card Dealer

# 2/28/2023 
## Team Meeting #3 and Design Document
Spoke with Professor Gruev about how we should go about the PCB design. We went over the power subsystems and the wire connections on our current PCB design. He says that we should account for the different sizes of wires: thicker wires for high power and high current consumming comonents and vice versa.  In addition, he gave us recommendation for changes that we should make to the placement of components on the PCB. 

We finalized the second version of the design document, added a micrcontroller section as a subsystem of the device. This will allow the reader to get a better understanding of how each of the devices connect and interface to the micrcontroller. I also updated the photos of each of the subsystems. I thought it would be a good idea to showcase how each of the subsystems connect to their respective components and the microcontroller. Rohit and Adam have been working hard on the PCB design and Schematic, sharing with me all of the updates to the designs. They realized that we are not allowed to use a Development Board, so they changed the block diagram and PCB to fit the STM32 microcontroller. I added the new materials to the bill of materials

Adam and Rohit made several changes to the power subsystem and the microcontroller. They added new voltage regulators to regulate voltages down from 9 to 6,5,3.3V to power each of the other subsystems. In addition, they added extra components necessary to operate the STM32 Microcontroller that we bought for the new PCB design. This includes the Power switches, crystal clocks, connections to motor subsystem. 

![](images/new_schematic.png)

# 3/01/2023
## Design Review with Professor Mironenko
Presented our design document in front of Professor Mironenko. We got feedback in regards to the high level requirements. We have to make them clearer and more definitive. How do you define a "shuffled deck of cards". How to you determine the success of a test for dealing one card. We also got feedback on our design requirements and verification processes. We have to declare the difference between the unit testing or subsystem verification. There is a difference between testing whether or not a part works and if a subsystem's purpose is fulfillled. 

# 3/3/2023
## Parts Update #1
Currently, our team has ordered the main mechanical and electronic hardware components of the project, including the shuffler, motors, ultrasonic sensors, microcontroller, buttons, and batteries. In addition, our team has spoken with the ECEB Machine Shop to discuss the physical design and feasibility of the project. Finally, we placed our first order for the PCB. 

I am about to order the parts necessary to connect the devices together: resistors, capacitors, connections, 16MHz clock, switches, and fuses. 

[link](https://www.digikey.com/short/wtf5nz9c)

# 3/07/2023 
## PCB Update
Rohit and Adam submitted the Gerber and Drill files to be audited before it gets manufactured by PCBWay. The figure below is the 3D PCB design. 

![](images/3D_pcb_first_round.jpeg)

## Team Meeting #4
After discussing the dealing mechanism at the team meeting, we decided to replace the servo morot with a dc motor. This design change is motor suitable for the dealing mechanism because the arm and servo motion would not be as fluid of a motion as a continously rotating DC motor. It is also easier to control the direction and speed of the DC motor. With a new DC motor, we will need to add a new motor driver IC unit to interface between the microcontroller and the dealing DC motor via pwm control signal. 

# 3/12/2023
## Microcontroller Update Initial
Installed all the STM main tools for programming on STM32:
- STM32CubeMX
- STM32CubeIDE
- STM32CubeProg
- STM32CubeL4

[link to STM Installation instructions](https://wiki.st.com/stm32mcu/wiki/Category:Getting_started_with_STM32_:_STM32_step_by_step)

Planning on familiarizing myself with the IDE, and reviewing some C programming techniques. I will start with some videos on programming on the STM32 Microcontroller. Then I will write psuedo code. 

# 3/13/2023 - 3/19/2023 
## SPRING BREAK

# 3/19/2023
## Parts Update #2
After running our first checks on the switches, and fuses, our team realized that I ordered the wrong part sizes. In particular, the switches and fuses were incompatible with the PCB design. 

I am about to order new parts: switches and fuses.

[link](https://www.digikey.com/en/mylists/list/N1QAZQI0UM)

# 3/21/2023
## Team Meeting #5
Reviewed Design Document before submission. we discussed What are the changes we should make to the Requirements and Verificaiton Tables, High Level Requirements and schedule. In particular, we made some changes to the verification tables to make them more specific and detailed. 

# 3/22/2023 - 3/24/2023
## Design Document - Second Version Submitted
Our team finalized our design document (the second version). We re-worded several requirement to clarify what should be expected and how should we verify that the subsystem or individual component should operate to perform a given task. How do we evaluate a success/failure. What are the procedures we should take if a test fails. The format that we used was "Must be able to..." ____ "to ensure that..." ______. This clarified the "what do we want the component to do and why do we need to ensure that this is executed properly."

Submitted the second version of the Design Document for the Autonomous Card Dealer

# 3/25/2023 - 3/26/2023 
## Sensing Subsystem Research
Initial General Notes on Ultrasonic Sensors
- Ultrasonic Sensors utilize sonar to take measurements of the distance between the object in front of the device and the device itself. 
- transmitter (TRIG)
- receiver (ECHO). 
- dist = (v_sound) * (time elapsed) * (1/2)

Below are some characteristics of the sensing subsystem
- The ultrasonic sensor we purchased has a distance range of 2cm to 400cm (1in to 13ft). 
- The device must be able to sense players at least 0.5 meters away, the radius of a professional standard poker table. 
- The error on the device, with properly working code, is 3mm. 
- The maximum reading angle (the wideness of measurement) is < 15 degrees. 
- The trig 

Here is the Sensing Subsystem's connection to the microcontroller on the schematic

![](images/sensing_subsystem.png)

# 3/27/2023  
## MicroController Update. Initial Design Considerations
My goal is to create the non-primitive functions of the project. Ideally, these non-primitive functions will utilize the

The non-primitive functions include:
``` c
void confirm_players   (int num_players);
void shuffle_cards     (int num_cards_shuffled);
void deal_players      (struct players);
void deal_field        (struct field);
void deal_rest_of_cards(int num_rest_of_cards);
```

The non-primitive function above rely on several primitive functions. These primitive functions send and/or receiving digital input and output signal to the devices involved in the project

The primitive functions include:
``` c
void print_7SEGDISP(struct lcd_message);
void rotate_base(float d_angle, float time_to_rotate);
void shuffle();
void deal_one_card();
struct buttons_pressed_struct get_buttons_pressed();
```

# 3/28/2023
## Worked on Individual Progress Report
Worked on the Individual Progres Report today. 

# 3/29/2023
## Individual Progress Report Submitted
Submitted the Individual Progress Report

# 3/30/2023 
## Parts Update #3
After running our second checks on the resistors, capacitors, our team realized that I ordered the wrong part sizes again. In particular, the connection headers, resistors, capacitors, and switches. The sizes of the parts we ordered were too small, which would make it difficult to for us to solder them onto the PCB. In addition, we made changes to the PCB design to include a DC motor for the dealing mechanism. I will need to order another Motor Driver IC. 

I am about to order new parts: connection headers, resistors, capacitors, switches for microcontroller, and Motor Driver IC. 

[link](https://www.digikey.com/short/7b3dffvr)

# 4/01/2023 - 4/10/2023 
## Microcontroller Update #3
### Confirm Players
Implemented confirm_players. Not yet tested since we have not implemented the primitive functions for rotate base and get distance reading. Below is the flowchart and psuedocode for it. 

![updated confirm players](images/confirm_players_flowchart.png)

``` c
/**
 * void confirm_locations(int num_players)
 * Input: num_players
 * Output: 1 if successful, 0 if unsucccessful
 * Description: 
 * Iterate through each player's angular positions
 * - set the servo motor position to ang_pos 
 * - utilize sensor subsystem reading to confirm player's existence
 *      - flash the player # (player we are waiting for)
 *      - get sensor measurement (front_distance)
 *      - if measurement is within threshold, then hold the the player # as sysem ACK
 *          then repeat until all num_players have been confirmed
 **/
void confirm_locations(int num_players)  { // new version

    float ang_between_players_deg = ROTATING_BASE_MAX_ROTATION / num_players; // degrees
    float time_rotate = 0;                                                    // seconds
    int brightness = 0;                                                       // [0,255]

    // disable all motors
    disable_motors("all"); 
    
    // init servo motor
    rotate_base(CLOCKWISE, 0);

    /* confirm each players existence in the game */
    for (int player = 0; player < num_players; player++){

        // reset brightness for UI
        brightness = 0;

        // set servo motor to angular position + stop
        // time_rotate = (ang_between_players_deg / ROTATING_BASE_ANGULAR_SPEED) * SEC_TO_MILLISEC; // seconds
        rotate_base(CLOCKWISE, ang_between_players_deg);

        // get digits to priint to UI
        digits.DIGIT1 = player;
        digits.DIGIT2 = player;
        digits.DIGIT3 = player;
        digits.DIGIT4 = player;

        /* get reading (wait until player confirms their location) */
        while(1){

            // print to UI (part 1) BEGIN Flash Wrapper
            brightness += FULL_BRIGHTNESS / BRIGHTNESS_CHANGE_FREQUENCY; 
            if (brightness > FULL_BRIGHTNESS)  brightness = 0;
            flash_UI(digits, brightness);
            
            // check measurement, break if within threshold dist
            float measurement = ultrasonic_sensor_measurement(); 
            if (measurement < ULTRASONIC_SENSOR_THRESHOLD_DISTANCE)  {
                player_distances[player] = measurement;
                flash_UI(digits, FULL_BRIGHTNESS);
                HAL_Delay(DELAY_TIME_DONE_CONFIRM_1PLAYER_LOC); 
                break;
            }

            HAL_Delay(DELAY_TIME_CONFIRM_LOCATIONS);

        }
    }

    // reset servo motor to 0 degrees
    // time_rotate = ROTATING_BASE_MAX_ROTATION / ROTATING_BASE_ANGULAR_SPEED;
    // rotate_base(COUNTERCLOCKWISE, time_rotate);
    rotate_base(COUNTERCLOCKWISE, ROTATING_BASE_MAX_ROTATION);
    
    // disable rotating servo motor signal
    disable_motors("all"); // disable all motors
    return;
}
```

### Deal Players for a given number of players and the number of cards that needs to be dealt to each player
Implemented deal_players. Not yet tested since we have not implemented the primitive functions for rotate base and deal one card. Below is the flowchart and psuedocode for it. 

![](images/deal_players_flowchart.png)

``` c
/**
 * void deal_players(game_mode game)
 * Input: game mode
 * Ouput: none
 * Description:
 * (1) Deal Player's Cards
*/
void deal_players(struct players_struct curr_game)   {

    // --------------------------------------------------------------------
    // PART 1: Deal player's cards
    // --------------------------------------------------------------------
    float arc_between_players_deg = ROTATING_BASE_MAX_ROTATION / curr_game.num_players;
    float time_rotate = 0;

    // set servo motor to init_ang_pos
    time_rotate = (0 / ROTATING_BASE_ANGULAR_SPEED) * SEC_TO_MILLISEC;
    rotate_base(CLOCKWISE, time_rotate);

    // infinite while loop based on game
    for(int card = 0; card < curr_game.num_cards_per_player; card++){
        for(int player = 0; player < curr_game.num_players; player++){

            digits.DIGIT1 = player;
            digits.DIGIT2 = player;
            digits.DIGIT3 = card / 10;
            digits.DIGIT4 = card % 10;
            // float dang_pos = (arc_between_players_deg); 

            // print to UI (part 1) BEGIN Wrapper
            flash_UI(digits, HALF_BRIGHTNESS);

            /* deal a card */ 
            deal_one_card();
            
            // print to UI (part 2)
            flash_UI(digits, FULL_BRIGHTNESS);

            /* 
            rotate servo motor to ang_pos + stop (CW by dang_pos), 
            reset after last player (CCW by MAX_ROTATION)
            */
            if (player == curr_game.num_players - 1)  { // hit last player, reset
                // time_rotate = ROTATING_BASE_MAX_ROTATION / ROTATING_BASE_ANGULAR_SPEED;
                // rotate_base(COUNTERCLOCKWISE, time_rotate);
                rotate_base(COUNTERCLOCKWISE, ROTATING_BASE_MAX_ROTATION);
            } else {
                // time_rotate = arc_between_players_deg / ROTATING_BASE_ANGULAR_SPEED;
                // rotate_base(CLOCKWISE, time_rotate);
                rotate_base(CLOCKWISE, arc_between_players_deg);
            }

            // print to UI (part 3) END Wrapper
            flash_UI(digits, ZERO_BRIGHTNESS);
        }
    }

    // reset servo motor to 0 degrees
    // time_rotate = 0 * ROTATING_BASE_ANGULAR_SPEED; // already reset
    // rotate_base(COUNTERCLOCKWISE, time_rotate);
    rotate_base(COUNTERCLOCKWISE, 0);
}
```

### Deal Field / Setup Field for a given Game Mode
Implemented deal_field. Not yet tested since we have not implemented the primitive functions for rotate base and get distance reading. Below is the flowchart and psuedocode for it. 

![](images/deal_field_flowchart.png)

``` c
/**
 * void deal_field(game_mode game)
 * Input: game mode
 * Ouput: none
 * Description:
 * (2) Deal Field Cards / Setup Field
*/
void deal_field(struct field_struct curr_game)  {
    // --------------------------------------------------------------------
    // PART 2: Deal the Field cards (setup field)
    // --------------------------------------------------------------------
    float time_rotate = 0;
    float dang_pos = 0;
    int direction;

    // disable all motors
    disable_motors("all");

    // set servo motor to init_ang_pos
    time_rotate = 0 / ROTATING_BASE_ANGULAR_SPEED;
    rotate_base(CLOCKWISE, time_rotate);
    
    // infinite while loop based on game
    for (int pos_idx = 0; pos_idx < MAX_NUM_FIELD_POS; pos_idx++){

        // rotate servo motor counter-clockwise to ang_pos
        if (pos_idx == 0){
            time_rotate = 0 / ROTATING_BASE_ANGULAR_SPEED;
            rotate_base(CLOCKWISE, time_rotate);
        } else {
            dang_pos = curr_game.angular_positions[pos_idx] - curr_game.angular_positions[pos_idx-1];
            time_rotate = dang_pos / ROTATING_BASE_ANGULAR_SPEED;
            if (dang_pos > 0){
                rotate_base(CLOCKWISE, time_rotate);
            } else { // go counter clockwise
                rotate_base(COUNTERCLOCKWISE, time_rotate);
            }
        }

        // deal cards at selected angular position
        for (int card = curr_game.num_cards_dealt[pos_idx]; card > 0; card--){
            
            digits.DIGIT1 = pos_idx;
            digits.DIGIT2 = pos_idx;
            digits.DIGIT3 = card;
            digits.DIGIT4 = card;

            // print to UI (part 2) 
            flash_UI(digits, FULL_BRIGHTNESS);

            /* deal a card */ 
            enable_dealing_motor();
            deal_one_card();
            disable_dealing_motor();

            // print to UI (part 3) END Wrapper
            flash_UI(digits, ZERO_BRIGHTNESS);
        }
    }

    // reset servo motor to 0 degrees
    time_rotate = curr_game.angular_positions[MAX_NUM_FIELD_POS-1] * ROTATING_BASE_ANGULAR_SPEED;
    rotate_base(COUNTERCLOCKWISE, time_rotate);
}
```

### Deal the Rest of Cards given the number of cards left

Implemented deal_rest_of_cards. Not yet tested since we have not implemented the primitive functions for rotate base and get distance reading. Below is the flowchart and psuedocode for it. 

![](images/deal_rest_of_cards_flowchart.png)

``` c
/**
 * void deal_field(game_mode game)
 * Input: game mode
 * Ouput: none
 * Description:
 * (3) Deal Rest of Cards
*/
void deal_rest(int rest_of_cards)  {
    // --------------------------------------------------------------------
    // PART 3: Deal rest of cards
    // --------------------------------------------------------------------
    float time_rotate = 0;
    int dig_10; int dig_1;

    // disable all motors
    disable_motors("all");

    // set servo motor to init_ang_pos
    time_rotate = 0 * ROTATING_BASE_ANGULAR_SPEED;
    rotate_base(CLOCKWISE, time_rotate);

    // deal cards at selected angular position
    for (int card = rest_of_cards; card > 0; card--){
        
        dig_10 = (int) card / 10; 
        dig_1 = card % 10;

        digits.DIGIT1 = dig_10;
        digits.DIGIT2 = dig_1;
        digits.DIGIT3 = dig_10;
        digits.DIGIT4 = dig_1;

        // print to UI (part 2) 
        flash_UI(digits, FULL_BRIGHTNESS);

        /* deal a card */ 
        deal_one_card();

        // print to UI (part 3) END Wrapper
        flash_UI(digits, ZERO_BRIGHTNESS);
    }

} 
```




# 4/11/2023 - 4/13/2023 
## MicroController Update
Ultrasonic Sensor Driver
Below are the set of instructions to implement the main driver function (get measurement) of the Ultrasonic Sensor
- (1) The TRIG pin emits a 40kHz sound. 
- (2) The vibrations of sound resonate through the air, bounces off an object or surface, back towards the ultrasonic receiver. 
- (3) The ECHO pin receives a pulse
- (4) We must measure the time between the initial transmission output signal and final reception input signal. 
- (5) To calculate the distance to the object or surface in front of the ultrasonic sensor, 
    - v_sound is the speed of sound (343m/s), 
    - t_rec is the final count on the timer when the echo pin reads a high signal, 
    - t_trans is the initial count on the timer when the trigger pin is pulsed high. 
- (6) We can utilize the following function: 
    - dist = (v_sound)    * (time elapsed)               * (1/2)
    - dist = (34.3 cm/ms) * (t_recieved - t_transmitted) * (1/2)  

[link to HC-SR04 datasheet](https://cdn.sparkfun.com/datasheets/Sensors/Proximity/HCSR04.pdf)

![](images/HC-SR04.png)

# 4/14/2023 
## PCB Update
Went to ECEB to help out with soldering. I mostly continued running code for the microcontroller. Trying to get a better understanding of how to get testable code for the microcontroller that we bought. While we were soldering, we ran into a problem while soldering the microcontroller: some of the microcontroller pins were bent in the process. In this event, I ordered 3 new Microcontrollers just in case we need to replace the microcontroller. 

## Parts Update #4
After running tests on the PCB, our team had a difficult time with soldering the microcontroller onto the PCB. We wanted to ensure that we had some insurance microcontrollers just in case we need to replace the PCB or simply replace the micrcontroller on the current PCB. 

I am about to order new parts: 3x STM32 Microcontroller. ARM® Cortex®-M3 STM32F1 Microcontroller IC 32-Bit Single-Core 72MHz 64KB (64K x 8) FLASH

[link](https://www.digikey.com/en/products/detail/stmicroelectronics/STM32F103C8T6/1646338?utm_adgroup=STMicroelectronics&utm_source=google&utm_medium=cpc&utm_campaign=Dynamic%20Search_EN_Focus%20Suppliers&utm_term=&utm_content=STMicroelectronics&gclid=EAIaIQobChMIjc3lhJKq_gIVYHxvBB3nDwS0EAAYAiAAEgJvlPD_BwE)

# 4/15/2023 - 4/17/2023 
## PCB Update
Went to the ECEB today to help out with soldering. Adam and Rohit were able to solder all of the rest of the parts (resistors, capacitors, other small connecting components) and began testing. While running tests, they realized that the parts that I ordered were either too small or too big to fit the footprints of the PCB. They tested the continuity of the PCB with all of the parts soldered on, and were not receiving any voltage readings. At this point, we decided that we will continue the project without using a our PCB design. 

Instead, we plan on breadboarding most of the Schematic. We would at least be able to show proof of concept for the curcuit we designed by mimicing the design onto a breadboard. We will utilize microcontroller development board (NUCLEO-F4LLRE) to write and test microcontroller code more effectively. 

## MicroController Update. 
Since we are no longer using the STM32 Microcontroller, the pin layout and IOC configuration that we originally wrote with will have to be switched to the configurtion of the NUCLEO-F4LLRE development board (uses the same microcontroller, just has a different IOC). 

## MicroController Update. 
After switching the microcontroller from the STM32Microcontroller on the PCB to the STM32Microcontroller on the Development Board, we ran into an issue where the development board may have gotten fried up. We noticed that the development board's microcontroller chip has overheated. I beleive the cause of the board getting overheated was that we plugged the power subsystem to a power generator. Maybe we did not adjust the current setting on the generator, and we may have shorted the development board's pins to a high current source. Luckily, Adam and Rohit's roomate had an extra development board. They diagnosed the problem and reverified which of the curcuit components were not damaged. They double checked and noticed that the power subsystem was fine, thus we can still use them with the new microcontroller development board. 

## Ultrasonic Sensor Driver Verification Method
In order to verify the accuracy of the ultrasonic sensor, we can place an object X cm away from the device, and compare the expected time elapsed time elapsed between the first TRIG pin high and the last ECHO pin low. Below is a set of distances (in cm) and their expected time elapsed (in ms). These calculations are made by solving for time_elapsed in the dist = v_sound * (time_elapsed) * (1/2).

Expected Time Elapsed = dist * 2 / v_sound

meas_dist ...   Expected Elapsed Time 
- 10      ...     0.5831
- 20      ...     1.1662
- 30      ...     1.7493
- 40      ...     2.3324
- 50      ...     2.9155

## Microcontroller Update.
Our goal today was to run tests to get buttons, shuffling, dealing, and LCD code working. I researched a bit on how the LCD works. This implementation I made was wrong because it did not acount for how data and commands are sent to the LCD display's processor. Rohit was able to get the code to run for the LCD using a 4 bit data line and without the I2C protocal. I then used the template of the LCD driver to begin writing a generalize Driver for any device. i.e. buttons, sensors, motors. Adam began working on the Fritzing Diagram so that we can have a verification process to troubleshoot any problems if we ever switch any wires on the breadboard. 

# 4/18/2023
## Mock Demo 1:00pm
Our team was given the chance to have a mock demonstration today. We showcased the working features of the Autonomous Card Dealer to Nikhil. We took advice from Nikhil, who told us how the demo will work, what will be the sequence of tests that we should showcase, and what should we expect at the final demo. This was the best way to prepare for the final demonstration.  

# 4/18/2023 
## Parts Update #5
After running tests on the PCB, our team had a few problems with running verification processes on the motor drivers. We came to the conclusion that we will need new motor drivers in the case that the motor subsysem's motor drivers do not work for demo. We wanted to ensure that we had some insurance Motor Driver ICs just in case we neec to replace the PCB or simply replace the motor driver ICs on the current PCB. 

I am about to order new parts: 3x Motor Driver IC. IC MTR DRV BIPOLAR 4.5-36V 16DIP

[link](https://www.digikey.com/en/products/detail/texas-instruments/L293DNE/379724?utm_adgroup=Texas%20Instruments&utm_source=google&utm_medium=cpc&utm_campaign=PMax%20Shopping_Supplier_Texas%20Instruments&utm_term=&utm_content=Texas%20Instruments&gclid=CjwKCAiAxvGfBhB-EiwAMPakqnWpm-_ZlTXT7YPZVRugEOxJ5xa7mcC-QNytpMWNWKuYV-7UFO-ikRoCKJsQAvD_BwE)

# 4/19/2023 
## Microcontroller Update.
Our Team decided that since the Final Demo and Presentation is close to date, we should prioritize implementing the basic functionality of the device instead of trying to integrate the "confirm players, shuffle, deal players, deal field, deal rest of cards" sequence that I had originally planned for. 
- Change number of players. 
- Change game mode
- Initiate Shuffling Sequence
- Initiate Dealing Sequence (scan for players and deal cards)

It is much more simple to unit test the functionality of the device by executing commands based on the button presses. I had originally thought that we coult have an increase, decrease, next, and start button in which the user could increase and decrease a setting, change to the next setting, and then run press "START" when we are satisfied with the settings we entered. I was inspired by the Ledger device, which utilizes two buttons. With this method, the user can enter their desired settings. The device would store all of these variables in a game_mode struct (consisting of a name, number of cards shuffled, player struct, field struct, number of cards). Then the ACD will use this game struct to execute the "confirm players, shuffle, deal players, deal field, deal rest of cards" sequence thoroughly. This will allow us to abstract any dealing sequence for any given card game for any number of cards for any number of players. 

Instead, we made the decision that we will be using the buttons to initiate the shuffling and dealing sequences. (1) the number of cards shuffled is preset to 52 (2) if you press any of the settings buttons (num_players and game_mode) and the variable has reached a max, then the variable is circled back to 0 (3) the field setups for each game mode is preset based on the preset game modes. This will make it easier to showcase the functinoality of the ACD during the demonstration

## Final Paper Update Draft 1
I began the first draft of the Final Paper today. I started off by downloading the template off of the UIUC ECE 445 Final Paper website. I was able to finish the Abstract portion of the paper.

# 4/21/2023 
## Team Contract Fulfillment
Our team was able to finish the Team Contract Fulfillment. We recycled some of the content from the original contract, and took note of how we worked as a team. We submitted the Team Contract Fulfillment

# 4/22/2023 
## Final Paper Update Draft 1
Added first draft of the User Instructions, Microcontroller Setup Procedures, and code segments to the Introductions portions of the paper. It gives the user a instruction manual on how to utilize the device, what to expect from each of the actions that the user makes, and how to manipulate the pre-programmed games. 

# 4/23/2023
## MicroController Update.
I wanted to learn about coding methods for embedded systems software. I tried to abstract some of the drivers for each of the devices that the microcontroller interfaces with. In particular, I used a template from the example device drivers from the STM32 Wiki and ControllersTech. They were able to abstract the device and its communication protocals using any given microcontroller confirguation and pin layout. In the same way, I want to be able to create drivers that control communications between the devices in the ACD and a chosen microcontroller. 

The non-primitive function above rely on several primitive functions. These primitive functions send and/or receiving digital input and output signal to the devices involved in the project

The primitive functions include:
``` c
void print_LCD     (struct lcd_message);
void rotate_base   (rotatingBase_HandleTypeDef * rotating_base, float d_angle);
void shuffle       (shuffler_HandleTypeDef * shuffler);
void deal_one_card (dealer_HandleTypeDef * dealer);
struct buttons_pressed_struct get_buttons_pressed(buttons_HandleTypeDef * buttons);
```

For the Shuffler Pins
```c
/********************** Shuffler Pin Setup *********************/
#define shuffler_PortType GPIO_TypeDef*
#define shuffler_PinType uint16_t

typedef struct {

	shuffler_PortType right_motor_port1;  // GPIOC, GPIO_PIN_4, GPIO_PIN_RESET
	shuffler_PinType  right_motor_pin1;   

    shuffler_PortType right_motor_port2;  // GPIOC, GPIO_PIN_5, GPIO_PIN_SET
	shuffler_PinType  right_motor_pin2;

	shuffler_PortType left_motor_port1;   // GPIOA, GPIO_PIN_6, GPIO_PIN_RESET
	shuffler_PinType  left_motor_pin1;

	shuffler_PortType left_motor_port2;   // GPIOA, GPIO_PIN_7, GPIO_PIN_SET
	shuffler_PinType  left_motor_pin2;

    //  will need to include the timer TIM1->CCR1 for the shuffler struct... dont know how tho
    //  will need to include the timer TIM1->CCR2 for the shuffler struct... dont know how tho

} shuffler_HandleTypeDef;

```

For the Dealer Pins
```c
/********************** Dealer DC Motor Pin Setup *********************/
#define dealer_PortType GPIO_TypeDef*
#define dealer_PinType uint16_t

typedef struct {

	dealer_PortType dealer_port1; // GPIOB, GPIO_PIN_5
	dealer_PinType dealer_pin1;

	dealer_PortType dealer_port2; // GPIOC, GPIO_PIN_7
	dealer_PinType dealer_pin2;

    //  will need to include the timer TIM3->CCR4 for the shuffler struct... dont know how tho


} dealer_HandleTypeDef;

```

For the Ultrasonic Sensor Pins
```c
/********************** Ultrasonic Sensor Pin Setup *********************/
#define ultrasonicSensor_PortType GPIO_TypeDef*
#define ultrasonicSensor_PinType uint16_t
typedef struct {

	ultrasonicSensor_PortType TRIG_PORT; // (TRIG_PORT, TRIG_PIN) OR (GPIOA, GPIO_PIN_9)
	ultrasonicSensor_PinType TRIG_PIN;

	ultrasonicSensor_PortType ECHO_PORT;
	ultrasonicSensor_PinType ECHO_PIN;

} ultrasonic_HandleTypeDef;
```

For LCD
```c
/********************* Liquid Crystal Disokay Pin Setup *********************/
#define Lcd_PortType GPIO_TypeDef*
#define Lcd_PinType uint16_t

typedef enum {
	LCD_4_BIT_MODE,
	LCD_8_BIT_MODE
} Lcd_ModeTypeDef;

typedef struct {
	Lcd_PortType * data_port;
	Lcd_PinType * data_pin;

	Lcd_PortType rs_port;
	Lcd_PinType rs_pin;

	Lcd_PortType en_port;
	Lcd_PinType en_pin;

	Lcd_ModeTypeDef mode;

} Lcd_HandleTypeDef;
```

For Buttons
```c
/********************** Buttons Pin Setup *********************/
typedef struct  {
    uint8_t INCREASE;
    uint8_t DECREASE;
    uint8_t NEXT;
    uint8_t START;

} buttons_pressed_struct;

#define buttons_PortType GPIO_TypeDef*
#define buttons_PinType uint16_t

typedef struct {

	buttons_PortType INCREASE_BUT_PORT;
	buttons_PinType  INCREASE_BUT_PIN;

	buttons_PortType DECREASE_BUT_PORT;
	buttons_PinType  DECREASE_BUT_PIN;

	buttons_PortType NEXT_BUT_PORT;
	buttons_PinType  NEXT_BUT_PIN;

	buttons_PortType START_BUT_PORT;
	buttons_PinType  START_BUT_PIN;

} buttons_HandleTypeDef;
```

## Final Paper Update Draft 1
Began writing the first draft of the Design Requirements and Verifications. Added Cost of Materials and Resources section

# 4/24/2023 
## Final Paper Update Draft 1
Finished writing the first draft of the Concolusion of the Final Paper. Finalized Cost and Bill of Materials for all of the parts that I ordered through the myECE website. All that is left to do is the Design Requirements and Verificaitons, Changes made since the original design, and R&V tables. 

In addition, we worked on the necessary documentation for the Final Demo, including the block diagram, high level requirements, R&V tables, changes we made from the original design. Adam and Rohit were able to get it printed out. 

# 4/25/2023
## Final Demo 5:00pm
Our team demoes our final demonstration today. success. 

# 4/28/2023
## Mock Final Presentation 8:00am
Our team was given the chance to have a mock presentation today. We took critical advice from Nikhil and the other TA present at the mock presentation. This was the best way to prepare for the final presentation.  

# 4/30/2023
## Final Presentation Update
The team met up today to work on the final presentation. We reduced some of the text, as recommended from the Mock Presentation. We also used cleaner images and designs to convey our work. 

# 5/01/2023
## Final Presentation 5:00pm
Our team presented our final presentation today. success

# 5/03/2023
## Final Paper Submitted
Submitted the Final Paper for the Autonomous Card Dealer:

# 5/04/2023
## Final ECE 445 Notebook Entry
Thank you to Adam Naboulsi, Rohit Chalamala, Nikhil Arora, and Professor Mironenko for an amazing semester
