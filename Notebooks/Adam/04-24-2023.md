# 4/24/2023
# Objectives
- Preparing for the demo

Today, we met up to prepare for the demo, creating any necessary documentation to bring with us. The document we made had the final block diagram, the high level requirements, and the requirements and verification tables along with changes made and tests done by subsystem. Once we finished writing everything up and reviewing it, we printed the documentation and were good to go for the demo.